<?php
session_start();
if (!isset($_SESSION['tenant'])) {
  header("location: loginUI.php");
}

?>
<!doctype html>
<html>

<head>

  <meta charset="utf-8">
  <title>Hong Kong Cube Shopping System</title>
  <!-- ajax css js library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <!-- font library -->
  <link href="https://fonts.googleapis.com/css2?family=Lato&family=Ubuntu&family=Volkhov&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Pangolin&display=swap" rel="stylesheet">
  <!--   
		font-family: 'Ubuntu', sans-serif;
		font-family: 'Pangolin', cursive;
		font-family: 'Volkhov', serif;
    haha
  -->
  <link rel="stylesheet" href="./CSS/TenantUI.css" />
  <script type="text/javascript" src="js/interface.js"> </script>
</head>

<body>


  <!-- container_browser-->
  <div class="flex-container">
    <div class="left-padding"> </div>

    <!-- container_main-->
    <div class="main-container flex-container">

      <!-- 1.navigation -->
      <nav class="navbar navbar-default" style="background-color: rgb(66, 156, 214); height: 52px;">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#" style="color:white;">Welcome <?php echo $_SESSION['tenant']['name'] ?></a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                  aria-expanded="false" style="background-color: rgb(231, 231, 231);">My Profile <span
                    class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><label for="tenantID">Your ID:</label><input type="text" name="tenantID" id="tenantID" readonly>
                  </li>
                  <li><label for="name">Your name: </label><input type="text" name="name" id="name" readonly></li>
                  <li role="separator" class="divider"></li>
                  <!-- <li><a href="#">Separated link</a></li> -->
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <button id="btnlogout" type="button" class="btn btn-default navbar-btn">Log Out</button>
            </ul>
          </div><!-- /.navbar-collapse -->

        </div>
      </nav>

      <!-- 2.Menu -->
      <div class="menubar">
        <div id="btnGoodsManagement" class="menu1">
          <ul>
            <li><a href="#">Goods Management</a></li>
          </ul>
        </div>
        <div id="btnOrderManagement" class="menu2">
          <ul>
            <li><a href="#">Order Management</a> </li>
          </ul>
        </div>
      </div>
      <!--3.MainContent  -->
      <div class="MainContent">
        <div class="contentTitle">Please select one function </div>
        <div id="panel_alert" class="panel panel-default" style="width:100%;background-color: #cb2431;display: none;">
          <div class="panel-body" style="font-size: 20px;color: white;">Once you delete a order, there is no going back.
            Please be certain.</div>
          <div class="panel-heading alertbody" style="font-size: 20px;">alert body</div>
          <div class="panel-footer" style="display: flex; justify-content: flex-end;">
            <button type="button" class="cancelcancelorder btn btn-default" style="display: block;">Cancel</button>
            <button type="button" class="confirmcancelorder btn btn-primary" style="display: block;">Confirm</button>
          </div>
        </div>

        <!-- #region goodsManagementUI -->
        <div id="goodsManagementUI" style="width: 100%; display: none;">
          <div id="panel_add" class="panel panel-default" style="width:100%; margin-top:20px;">
            <div class="panel-body text-center" style="font-size: 24px; cursor:pointer;">
              Add in new goods
            </div>
          </div>
          <div id="result" class="table_container">
            <!-- result div -->
          </div>
        </div>
        <!-- #endregion -->


        <div id="orderManagementUI" style="margin-top:20px;width: 100%; display: none;">
          <!-- result_ordersManagmentUI -->
        </div>
      </div>

    </div>
    <div class="right-padding"> </div>
  </div>


  <div id="testerror"></div>
</body>

</html>
<!-- btnEdit modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Update Goods</h4>
      </div>
      <div id="goodsdetails" class="modal-body">
        <table class="table">
          <tr>
            <td>Goods Name</td>
            <td><input type="text" id="goodsdetail-name"></td>
          </tr>
          <tr>
            <td>price</td>
            <td><input type="text" id="goodsdetail-price"></td>
          </tr>
          <tr>
            <td>quantity</td>
            <td><input type="text" id="goodsdetail-quantity"></td>
          </tr>
          <input type="hidden" id="goodsdetail-id">
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="updateDB_goods" type="button" class="btn btn-primary">Save Changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- btnAdd modal -->
<div id="modal_add" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add in goods</h4>
      </div>
      <div id="goodsdetails" class="modal-body">
        <table class="table">
          <tr>
            <td>Consignment Store ID</td>
            <td><select id="newgoods_consignmentStoreID">
                <!-- fetchStoreName() -->
              </select></td>
          </tr>
          <tr>
            <td>Goods Name</td>
            <td><input type="text" id="newgoods_goodsName"></td>
          </tr>
          <tr>
            <td>Price</td>
            <td><input type="text" id="newgoods_stockPrice"></td>
          </tr>
          <tr>
            <td>Quantity</td>
            <td><input type="text" id="newgoods_remainingStock"></td>
          </tr>
          <input type="hidden" id="goodsdetail-id">
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="insertDB_goods" type="button" class="btn btn-primary">Insert</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- javascript -->
<script type="text/javascript">
  $('#btnlogout').click(function () {
    window.location.href = "server/ser.logout.php";
  });


  $(document).ready(function () {
    setTenantProfile();
    setSelectOption();
    $(document).on("click", "#btnGoodsManagement", function () {
      fetchGoodsForTenant();
      showGoodsMangementUI();
    });
    $(document).on("click", "#btnOrderManagement", function () {
      fetchOrders();
      showOrderMangementUI();
    });

    /* #region  btn&times */
    $(document).on("click", ".cancelorder", function () {
      $('#panel_alert').fadeIn(25);
      $('.alertbody').html($(this).data('details'));
      var orderID = $(this).data('orderid');
      var panelid = "panel" + $(this).data('orderid');
      cancelcancelorderSetOnClick();
      $(document).on("click", ".confirmcancelorder", function () {
        $.ajax({
          url: "server/ser.tenant.deleteOrder.php",
          method: "POST",
          data: {
            action: "deleteOrder",
            orderID: orderID
          },
          success: function (data) {
            if (data == 'Cancel order success') {
              $('#' + panelid).fadeOut();
              $('#panel_alert').hide();
            } else if (data == 'Something went wrong') {
              alert(data);
            }
          }
        });
      });

    });
    /* #endregion */

    /* #region  btnAdd */
    $(document).on("click", "#panel_add", function () {
      $('#modal_add').modal('show');
    });
    /* #endregion */

    /* #region  btnEdit */
    $(document).on("click", ".editGoods", function () {
      $('#goodsdetail-name').val($(this).data('product_name'));
      $('#goodsdetail-price').val($(this).data('product_price'));
      $('#goodsdetail-quantity').val($(this).data('product_stockquantity'));
      $('#goodsdetail-id').val($(this).data('product_id'));
      $('#myModal').modal('show');
    });
    /* #endregion */

    /* #region  btnRemove */
    $(document).on("click", ".removeGoods", function () {
      var status = $(this).data("status");
      var goodsNumber = $(this).data("goodsnum");

      $.ajax({
        url: "server/ser.tenant.removeGoods.php",
        method: "POST",
        data: {
          action: "removeGoods",
          goodsNumber: goodsNumber,
          status: status
        },
        success: function (data) {
          fetchGoodsForTenant();
          alert(data);
        }
      });
    });
    /* #endregion */

    /* #region  btnSaveChanges */
    $(document).on("click", "#updateDB_goods", function (event) {
      event.preventDefault();
      var goodsName = $('#goodsdetail-name').val();
      var stockPrice = $('#goodsdetail-price').val();
      var remainingStock = $('#goodsdetail-quantity').val();
      var goodsNumber = $('#goodsdetail-id').val();
      if ($.trim(goodsName).length == 0 || $.trim(stockPrice).length == 0 || $.trim(remainingStock).length == 0) {
        alert("Empty textbox found.")
      } else {
        $.ajax({
          url: "server/ser.tenant.updateGoods.php",
          method: "POST",
          data: {
            action: "updateDB_goods",
            goodsName: goodsName,
            stockPrice: stockPrice,
            remainingStock: remainingStock,
            goodsNumber: goodsNumber
          },
          beforeSend: function () {
            $('#submit_siupdateDB_goodsgnup').html("updating");
          },
          success: function (data) {
            if (data == "Update Success.") {
              fetchGoodsForTenant();
              $('#myModal').modal('hide');
            }
            $('#updateDB_goods').html('Save changes');
            alert(data);
          }
        });
      }
    });
    /* #endregion */

    /* #region  btnInsert */
    $(document).on("click", "#insertDB_goods", function () {
      var consignmentStoreID = $('#newgoods_consignmentStoreID').val();
      var goodsName = $('#newgoods_goodsName').val();
      var stockPrice = $('#newgoods_stockPrice').val();
      var remainingStock = $('#newgoods_remainingStock').val();

      if ($.trim(consignmentStoreID).length == 0 || $.trim(goodsName).length == 0 || $.trim(stockPrice).length == 0 || $.trim(remainingStock).length == 0) {
        alert("Empty textbox found.")
      } else {
        $.ajax({
          url: "server/ser.tenant.insertGoods.php",
          method: "POST",
          data: {
            action: "insertDB_goods",
            consignmentStoreID: consignmentStoreID,
            goodsName: goodsName,
            stockPrice: stockPrice,
            remainingStock: remainingStock,
          },
          beforeSend: function () {
            $('#insertDB_goods').html("Inserting");
          },
          success: function (data) {
            if (data == "success") {
              fetchGoodsForTenant();
              $('#modal_add').modal('hide');
              $("#goodsdetails input[type='text']").val("");
            }else{
              alert(data);
            }
            $('#insertDB_goods').html('Insert');
          }
        });
      }
    });
    /* #endregion */



    /* #region  function */
    function fetchGoodsForTenant() {
      var action = "select";
      var tenantID = $('#tenantID').val();
      $.ajax({
        url: "server/ser.tenant.fetchGoods.php",
        method: "POST",
        data: {
          action: action,
          tenantID:tenantID
        },
        success: function (data) {
          $('#result').html(data);
        }
      })
    }

    function fetchOrders() {
      $.ajax({
        url: "server/ser.tenant.fetchOrders.php",
        method: "POST",
        data: {
          action: "fetchOrders"
        },
        success: function (data) {
          $('#orderManagementUI').html(data);
          //  $('#testerror').html(data);
        }
      })
    }

    function setTenantProfile() {
      $.ajax({
        url: "server/ser.tenant.fetchTenant.php",
        method: "POST",
        data: {
          action: "fetchTenant"
        },
        success: function (data) {
          data = JSON.parse(data);
          var tenantID = data.tenantID;
          var name = data.name;
          var password = data.password;
          $('#tenantID').val(tenantID);
          $('#name').val(name);
        }
      });
    }

    function showGoodsMangementUI() {
      $('#goodsManagementUI').css("display", "block");
      $('#orderManagementUI').css("display", "none");
      $('.contentTitle').html("Goods Management");
    }

    function showOrderMangementUI() {
      $('#goodsManagementUI').css("display", "none");
      $('#orderManagementUI').css("display", "block");
      $('.contentTitle').html("Order Management");
    }

    function cancelcancelorderSetOnClick() {
      $(document).on("click", ".cancelcancelorder", function () {
        $('#panel_alert').fadeOut(200);
      });
    }

    function setSelectOption() {
      var tenantID = "";
      $.ajax({
        url: "server/ser.tenant.fetchTenant.php",
        method: "POST",
        data: {
          action: "fetchTenant"
        },
        success: function (data) {
          tenantID = JSON.parse(data).tenantID;
          $.ajax({
            url: "server/ser.tenant.fetchStores.php",
            method: "POST",
            data: {
              tenantID: tenantID,
              action: "fetchStores"
            },
            success: function (data) {
              data = JSON.parse(data);
              $.each(data, function (key, value) {
                $('#newgoods_consignmentStoreID').append($('<option>').text(value['ConsignmentStoreName']).val(value['consignmentStoreID']));
              });
            }
          })
        }
      })
    };
    /* #endregion */
  });
</script>