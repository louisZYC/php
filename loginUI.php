<?php
session_start();
if (isset($_SESSION['tenant'])) {
  header("location: Home_Tenant.php");
}
if (isset($_SESSION['customer'])) {
  header("location: customer/home");
}
?>


<!doctype html>
<html>

<head>
  <title>Hong Kong Cube Shopping System</title>
  <!-- ajax css js library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <!-- font library -->
  <link href="https://fonts.googleapis.com/css2?family=Lato&family=Ubuntu&family=Volkhov&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Pangolin&display=swap" rel="stylesheet">
  <!--   
		font-family: 'Ubuntu', sans-serif;
		font-family: 'Pangolin', cursive;
		font-family: 'Volkhov', serif;

  -->
  <link rel="stylesheet" href="CSS/LoginUI.css" />
</head>

<body>


  <!-- Container -->
  <div class="loginFormcontainer">

    <div class="toggle">
      <a class="btnTenant" href="#" onclick="gotoTenantLogin()">Tenant Login</a>
      <a class="btnCustomer" href="#" onclick="gotoCustomerLogin()">Customer Login</a>
    </div>

    <!-- tenantLoginform -->
    <div id="tenantLoginform" class="loginForm">
      <form method="POST" class="elements">
        <h2>Tenant Login</h2>
        <input type="text" placeholder="Tenant ID" name="tenantid" id="tenantid" class="textbox"><br><br>
        <input type="password" placeholder="Password" name="password" id="tenantpassword" class="textbox"><br><br>
        <input type="button" value="Login" name=" " id="login_tenant" class="loginbutton">
        <div class="errormessage"><span id="errormessagetenant"></span></div>
      </form>
    </div>

    <!-- customerLoginform -->
    <div style="display: none;" id="customerLoginform" class="loginForm">
      <form method="POST" class="elements">
        <h2>Customer Login</h2>
        <input id="customeremail" type="text" placeholder="Email address" name="email" class="textbox"><br><br>
        <input id="customerpassword" type="password" placeholder="Password" name="customerpassword" class="textbox"><br><br>
        <input type="button" value="Login" name="login_customer" id="login_customer" class="loginbutton" style="background-color: rgba(255, 0, 0,1);">
        <button id="login_customer" type="button" class="btn btn-success" onclick="location.href='signup.php'">Signup</button>
        <div class="errormessage" style="font-size: 20px;"><span id="errormessagecustomer"></span></div>
      </form>
    </div>

  </div>

  <span id="useoftest"></span>
</body>

</html>


<!-- Java Script -->
<script type="text/javascript">
  $(document).ready(function() {
    //1.tenant's login button onClick
    $('#login_tenant').click(function() {
      var tenantid = $('#tenantid').val();
      var tenantpassword = $('#tenantpassword').val();
      if ($.trim(tenantid).length > 0 && $.trim(tenantpassword).length > 0) {
        $.ajax({
          url: "server/ser.login.php",
          method: "POST",
          data: {
            action: "tenantlogin",
            tenantid: tenantid,
            tenantpassword: tenantpassword
          },
          beforeSend: function() {
            $('#login_tenant').val("connecting...");
          },
          success: function(data) {
            $('#login_tenant').val("Login");
            if (data == 'tenantloginsuccess') {
              window.location.href = "Home_Tenant.php";
            }
            $('#errormessagetenant').html(data);
          }

        })
      } else {
        $('#errormessagetenant').html("Please input both ID and password.");
      }
    })

    //2. customer
    $('#login_customer').click(function() {
      var customeremail = $('#customeremail').val();
      var customerpassword = $('#customerpassword').val();
      if ($.trim(customeremail).length > 0 && $.trim(customerpassword).length > 0) {
        $.ajax({
          url: "server/ser.login.php",
          method: "POST",
          data: {
            action: "customerlogin",
            customeremail: customeremail,
            customerpassword: customerpassword
          },
          beforeSend: function() {
            $('#login_customer').val("connecting...");
          },
          success: function(data) {
            $('#login_customer').val("Login");
            $('#errormessagecustomer').html(data);
            if (data == 'customerloginsuccess') {
              // window.location.href = "home_customer.php";
              window.location.href = "customer/home";
            }
          }

        })
      } else {
        $('#errormessagecustomer').html("Please input both email and password.");
      }
    })
  });


  /* #region  toggle button onclick */
  function gotoCustomerLogin() {
    var customer = document.getElementsByClassName("btnCustomer");
    customer[0].style.backgroundColor = "rgba(255, 0, 0,1)";
    var tenant = document.getElementsByClassName("btnTenant");
    tenant[0].style.backgroundColor = "rgba(54, 10, 10,0.2)";

    var customerloginform = document.getElementById("customerLoginform");
    customerloginform.style.display = "flex";
    var tenantLoginform = document.getElementById("tenantLoginform");
    tenantLoginform.style.display = "none";
  }

  function gotoTenantLogin() {
    var tenant = document.getElementsByClassName("btnTenant");
    tenant[0].style.backgroundColor = "rgba(66, 156, 214,1)";
    var customer = document.getElementsByClassName("btnCustomer");
    customer[0].style.backgroundColor = "rgba(54, 10, 10,0.2)";

    var customerloginform = document.getElementById("customerLoginform");
    customerloginform.style.display = "none";
    var tenantLoginform = document.getElementById("tenantLoginform");
    tenantLoginform.style.display = "flex";
  }
  /* #endregion */
</script>
