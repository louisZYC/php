<?php
$output = '';
$action = '';
if (isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
  echo "nice to meet you";
}

if ($action == "fetchOrders") {
  // define variables
    require_once("../connections/conn.php");
    session_start();
    $tenantID = $_SESSION['tenant']['tenantid'];
    // echo var_dump($_SESSION);
    // exit();
    $sql = "SELECT o.orderID , o.orderDateTime, c.firstName, s.address, o.status, o.totalPrice\n"

    . "FROM customer as c\n"

    . "INNER JOIN orders as o ON c.customerEmail = o.customerEmail\n"

    . "INNER JOIN shop as s ON o.shopID = s.shopID\n"

    . "INNER JOIN consignmentstore as cs on cs.consignmentStoreID = o.consignmentStoreID\n"

    . "INNER JOIN tenant as t on t.tenantID = cs.tenantID\n"

    . "WHERE t.tenantID = '".$tenantID."'";


if ($result = $conn->query($sql)) {
    if ($result->num_rows > 0) {
        $rows_orders = array();
        while ($row = $result->fetch_assoc()) {
            array_push($rows_orders, $row);
        };

        $output .= "
        <div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>
        ";
        // foreach loop
        foreach ($rows_orders as $onerow) {
          $orderID = $onerow['orderID'];
          $listOfOrderItems = getListOfOrdersItem($orderID,$conn);

            switch ($onerow['status']) {
                case 1:
                    $status = 'Delivery';
                    break;
                case 2:
                    $status = 'Awaiting';
                    break;
                case 3:
                    $status = 'Completed';
                    break;
            }
            $output .= "
            <div class='panel panel-default' id='panel".$onerow['orderID']."'>

               <button type='button' data-orderid= '".$onerow['orderID']."' data-details=' Order ID :    ".$onerow['orderID']."    Details: ".$onerow['orderDateTime']."   ".$onerow['firstName']."   ".$status."' class='cancelorder close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true' style='font-size: 2em;'>&times;</span>
              </button>
              <div class='panel-heading' role='tab' id='heading".$onerow['orderID']."'>
                <h4 class='panel-title'>
                    <table class='datagripview' role='button' data-toggle='collapse' data-parent='#accordion' href='#collapse".$onerow['orderID']."'
                    aria-expanded='true' aria-controls='collapse".$onerow['orderID']."'>
                        <tr style='pointer-events: none;font-family:'Pangolin', cursive;'>
                        <th class='goodlist_th noBorder' style='font-size:16px; width:10%;'>Order ID</th>
                        <th class='goodlist_th noBorder' style='font-size:16px; width:10%;'>Order Date</th>
                        <th class='goodlist_th noBorder' style='font-size:16px; width:13%;'>Customer Name</th>
                        <th class='goodlist_th noBorder' style='font-size:16px; width:37%;'>Shop Address</th>
                        <th class='goodlist_th noBorder' style='font-size:16px; width:15%;'>Order Status</th>
                        <th class='goodlist_th noBorder' style='font-size:16px; width:15%;'>Total Price</th>
                      </tr>
                      <tr style='pointer-events: none;'>
                        <td class='goodlist_td' style='font-size:16px;'>" . $orderID . "</td>
                        <td class='goodlist_td' style='font-size:16px;'>" . $onerow['orderDateTime'] . "</td>
                        <td class='goodlist_td' style='font-size:16px;'>" . $onerow['firstName'] . "</td>
                        <td class='goodlist_td' style='font-size:16px;'>" . $onerow['address'] . "</td>
                        <td class='goodlist_td' style='font-size:16px;'>" .$status. "</td>
                        <td class='goodlist_td' style='font-size:16px;'>" . $onerow['totalPrice'] . "</td>
                      </tr>
                    </table>
                </h4>
              </div>
              <!-- end -->
              <div id='collapse".$onerow['orderID']."' class='panel-collapse collapse' >
                <div class='panel-body'>
                  <table class='datagripview'>
                    <tr style='font-family:'Pangolin', cursive;'>
                      <th class='goodlist_th' style='font-size:16px;'>Goods Number</th>
                      <th class='goodlist_th' style='font-size:16px;'>Goods Name</th>
                      <th class='goodlist_th' style='font-size:16px;'>Quantity</th>
                      <th class='goodlist_th' style='font-size:16px;'>Selling Price</th>
                    </tr>
                    ";
            foreach($listOfOrderItems as $orderitem){
              $output.="
              <tr>
                <td class='goodlist_td' style='font-size:16px;'>".$orderitem['goodsNumber']."</td>
                <td class='goodlist_td' style='font-size:16px;'>".$orderitem['goodsName']."</td>
                <td class='goodlist_td' style='font-size:16px;'>".$orderitem['quantity']."</td>
                <td class='goodlist_td' style='font-size:16px;'>".$orderitem['sellingPrice']."</td>
              </tr>";
            }
           
                    
             $output .="</tr>
                  </table>
                </div>
              </div>
            </div>
            ";
        }//end foreachloop

        $output .= "</div>";
    }
    echo $output;
    /* free result set */
    // $result->close();
} else {
    die("failed query():" . $conn->error);
}

}


function getListOfOrdersItem(&$orderID) {


  $sql = "SELECT oi.goodsNumber, g.goodsName, oi.quantity, oi.sellingPrice"

    . " FROM orderitem AS oi"

    . " INNER JOIN goods AS g ON g.goodsNumber = oi.goodsNumber"

    . " WHERE orderID = ?";
    $conn = db();
    $statement = $conn->stmt_init();
    if(!$statement->prepare($sql)){
        die('prepare() failed: ' . $conn->error);
    }
    if (!$statement->bind_param("i",$orderID)) {
        die('bind_param() failed: ' . $statement->error);
    }
    if(!$statement->execute()){
        die('execute() failed: ' . $statement->error);
    }

    $result = $statement->get_result();
    while($result->fetch_array()){
      $listOfOrderItems = $result;
    };
  return $listOfOrderItems;
}



function db () {
  static $conn;
  if ($conn===NULL){ 
    $domain = "127.0.0.1";
    $username = "root";
    $password= "";
    $database = "projectdb";
    $conn = mysqli_connect($domain, $username, $password,$database)or die("Failed connection: ".mysqli_connect_error());
  }
  return $conn;
}