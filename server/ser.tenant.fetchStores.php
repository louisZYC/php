<?php
$output = '';
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'fetchStores') {
        // connects to db
        require_once("../connections/conn.php");
        session_start();
        // declare php variable
        $tenantID = mysqli_escape_string($conn, $_SESSION['tenant']['tenantid']);



        // selectDB_tenant
        $statement = $conn->stmt_init();
        $sql = "select * from consignmentstore where tenantID = ?";
        if (!$statement->prepare($sql)) {
            die('prepare() failed: ' . $conn->error);
        }
        if (!$statement->bind_param("s", $tenantID)) {
            die('bind_param() failed: ' . $statement->error);
        }
        if (!$statement->execute()) {
            die('execute() failed: ' . $statement->error);
        }

        $result = $statement->get_result();
        while($data = $result->fetch_assoc()){
            $statistic[] = $data;
        };
        $output = json_encode($statistic);
        $statement->close();

        // echo $output;
        echo $output;
    }
} else {
    echo 'nice to meet you';
}
