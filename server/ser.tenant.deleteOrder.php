<?php 
$output='';
$action ='';
if(isset($_POST['action'])){
    $action=$_POST['action'];
}

if($action == 'deleteOrder'){
    //connect to db
    require_once("../connections/conn.php");

    //declare php var
    $orderID = mysqli_escape_string($conn, $_POST['orderID']);

    //update DB
    $sql = "DELETE FROM orderitem  WHERE orderID = ?";
    $statement = $conn->stmt_init();
    if(!$statement->prepare($sql)){
        die('prepare() failed: ' . $conn->error);
    }
    if (!$statement->bind_param("i",$orderID)) {
        die('bind_param() failed: ' . $statement->error);
    }
    if(!$statement->execute()){
        die('execute() failed: ' . $statement->error);
    }
    $statement->store_result();
    $affectedrows = $statement->affected_rows;
    if($affectedrows>0){
        $statement->close();
        $output = deleteOrder($orderID);
        echo $output;
    }
    // }
}else{
    $output = 'nice to meet you';
    echo $output;
}


function deleteOrder(&$orderID) {
    $sql = "DELETE FROM orders  WHERE orderID = ?";
      $conn = db();
      $statement = $conn->stmt_init();
      if(!$statement->prepare($sql)){
          die('prepare() failed: ' . $conn->error);
      }
      if (!$statement->bind_param("i",$orderID)) {
          die('bind_param() failed: ' . $statement->error);
      }
      if(!$statement->execute()){
          die('execute() failed: ' . $statement->error);
      }
      $statement->store_result();
      $affectedrows = $statement->affected_rows;
      if($affectedrows>0){
        $statement->close();
        $output = 'Cancel order success';
        return $output;
      }else{
          $output ='Something went wrong';
          return $output;
      }
      
  }




function db () {
    static $conn;
    if ($conn===NULL){ 
      $domain = "127.0.0.1";
      $username = "root";
      $password= "";
      $database = "projectdb";
      $conn = mysqli_connect($domain, $username, $password,$database)or die("Failed connection: ".mysqli_connect_error());
    }
    return $conn;
  }
