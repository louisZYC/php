<?php
session_start();
$output = '';

//declare php variable
$aciton = '';
$isCorrectid = false;

if (isset($_POST['action'])) {
    $aciton = $_POST['action'];
    //connect to database
    require_once("../connections/conn.php");
    //0.init statement
    $statement = $conn->stmt_init();
} else {
    $output = "nice to meet you";
    echo $output;
}

if ($aciton == 'tenantlogin') {
    //declare php variable 
    $tenantid = mysqli_escape_string($conn, $_POST['tenantid']);
    $tenantpassword = $conn->escape_string($_POST['tenantpassword']);

    //validate account
    $sql = "select * from tenant where tenantID = ?";
    if (!$statement->prepare($sql)) {
        die('prepare() failed: ' . $conn->error);
    }
    if (!$statement->bind_param("s", $tenantid)) {
        die('bind_param() failed: ' . $statement->error);
    }
    if (!$statement->execute()) {
        die('execute() failed: ' . $statement->error);
    }
    $result = $statement->get_result();
    $row = $result->num_rows;
    if ($row == 0) {
        $output = "This tenant id does not exist.";
        echo $output;
    } else {
        $isCorrectid = true;
    }
    $statement->close();



    if ($isCorrectid == true) {
        $statement = $conn->stmt_init();
        $sql = "select * from tenant where tenantID = ? and password = ?;";
        if (!$statement->prepare($sql)) {
            die('prepare() failed: ' . $conn->error);
        }
        //3.bind parameter
        if (!$statement->bind_param("ss", $tenantid, $tenantpassword)) {
            die('bind_param() failed: ' . $statement->error);
        }
        if (!$statement->execute()) {
            die('execute() failed: ' . $statement->error);
        }
        $result = $statement->get_result();
        $row = $result->num_rows;
        if ($row > 0) {
            $data = $result->fetch_assoc();
            $tenantinfo = array(
                'tenantid' => $data["tenantID"],
                'name' => $data["name"],
                'password' => $data["password"]
            );
            $_SESSION['tenant'] = $tenantinfo;
            $output = "tenantloginsuccess";
            echo $output;
        } else {
            $output = "Incorrect password.";
            echo $output;
        }
    }
}



// customer Login
if ($aciton == 'customerlogin') {
    $customeremail = mysqli_escape_string($conn, $_POST['customeremail']);
    $customerpassword = $conn->escape_string($_POST['customerpassword']);

    //validate account
    $statement = $conn->stmt_init();
    $sql = "select * from customer where customerEmail = ?";
    if (!$statement->prepare($sql)) {
        die('prepare() failed: ' . $conn->error);
    }
    if (!$statement->bind_param("s", $customeremail)) {
        die('bind_param() failed: ' . $statement->error);
    }
    if (!$statement->execute()) {
        die('execute() failed: ' . $statement->error);
    }
    $result = $statement->get_result();
    $row = $result->num_rows;
    if ($row == 0) {
        $output = "The email has not been registered.";
        $isCorrectid = false;
        echo $output;
    } else {
        $isCorrectid = true;
    }
    $statement->close();


    $statement = $conn->stmt_init();
    if ($isCorrectid == true) {
        //1. declare sql
        $sql = "select * from customer where customerEmail = ? and password = ?;";
        //2. prepare sql
        if (!$statement->prepare($sql)) {
            die('prepare() failed: ' . $conn->error);
        }
        if (!$statement->bind_param("ss", $customeremail, $customerpassword)) {
            die('bind_param() failed: ' . $statement->error);
        }
        if (!$statement->execute()) {
            die('execute() failed: ' . $statement->error);
        }
        $result = $statement->get_result();
        $row = $result->num_rows;
        if ($row > 0) {
            $data = $result->fetch_assoc();
            $customerinfo = array(
                'customerEmail' => $data["customerEmail"],
                'firstName' => $data["firstName"],
                'lastName' => $data['lastName'],
                'phoneNumber' => $data['phoneNumber'],
                'password' => $data['password']
            );
            $_SESSION['customer'] = $customerinfo;
            $output = "customerloginsuccess";
            echo $output;
        } else {
            $output = "Incorrect password.";
            echo $output;
        }
    }
}
