<?php

session_start();
// unset($_SESSION['shoppingcart']);
// echo '<pre>';
// echo var_dump($_SESSION);
// echo '</pre>';
// exit();
$totalprice=0;
$output='';
$output.='<table class="table table-bordered" ">    
            <tr>
                <th>Name</th>
                <th>Quantity</th>
                <th>price</th>
                <th>Total</th>
                <th>Remove</th>
            </tr>
        ';
        


if(!empty($_SESSION["shoppingcart"])){
    $_SESSION['totalPrice'] = calShoppingcartTotalprice();
    foreach($_SESSION["shoppingcart"] as $keys => $values){
        $output.='
            <tr>
                <td>'.$values['goodsName'].'</td>
                <td>'.$values['quantity'].'</td>
                <td>'.$values['sellingPrice'].'</td>
                <td>'.$values['quantity']*$values['sellingPrice'].'</td>
                <td id="btnRemove" data-goodsnum ="'.$values['goodsNumber'].'"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
              </svg></td>
            </tr>
        ';
       // $totalprice+=($value["quantity"]*$value["price"]);
    }
    $output .='
        <tr>
            <td colspan="3" align="right">Total</td>
            <td>'.$_SESSION['totalPrice'].'</td>
        </tr>
    ';
}else{
    $output.='
        <tr>
            <td colspan="5" align="center">Empty cart</td>
        </tr>
    ';
}
$output.='</table>';
echo $output;

function calShoppingcartTotalprice(){
    $totalprice = 0;
    foreach($_SESSION["shoppingcart"] as $keys => $values){
       $totalprice += floatval($values['sellingPrice'])*floatval($values['quantity']);
    }
    return $totalprice;
}