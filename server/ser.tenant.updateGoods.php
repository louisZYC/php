<?php
$output = '';
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'updateDB_goods') {
        // connects to db
        require_once("../connections/conn.php");
        // declare php variable
        $goodsName = mysqli_escape_string($conn, $_POST['goodsName']);
        $stockPrice = mysqli_escape_string($conn, $_POST['stockPrice']);
        $remainingStock = mysqli_escape_string($conn, $_POST['remainingStock']);
        $goodsNumber = mysqli_escape_string($conn, $_POST['goodsNumber']);



        //1. validate format
        $isCorrectQuantity = false;
        $isCorrectPrice = false;
        if (!preg_match('/^[0-9]+(\.[0-9])?$/', $stockPrice)) {
            $output = 'Price neither be integer or one digit decimal';
            echo $output;
        } else {
            $isCorrectQuantity = true;
        }

        if ($isCorrectQuantity) {
            if (!preg_match('/^[0-9]+$/', $remainingStock)) {
                $output = 'Wrong quantity input';
                echo $output;
            } else {
                $isCorrectPrice = true;
            }
        }




        // updateDB_goods
        if ($isCorrectPrice  == true) {
            $statement = $conn->stmt_init();
            $sql = "UPDATE goods SET goodsName = ?, stockPrice = ?, remainingStock = ? WHERE goodsNumber = ?;";
            if (!$statement->prepare($sql)) {
                die('prepare() failed: ' . $conn->error);
            }
            if (!$statement->bind_param("sdii", $goodsName, $stockPrice, $remainingStock, $goodsNumber)) {
                die('bind_param() failed: ' . $statement->error);
            }
            if (!$statement->execute()) {
                die('execute() failed: ' . $statement->error);
            }

            $statement->store_result();//use of test
            $affectedrows = $statement->affected_rows;//use of test
            $output = 'Update Success.';
            echo $output;
            $statement->close();
        }
    }
} else {
    echo 'nice to meet you';
}
