<?php
$output = '';
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'signup') {

        // connects to db
        require_once("../connections/conn.php");
        // declare php variable
        $email = mysqli_escape_string($conn, $_POST['email']);
        $firstname = mysqli_escape_string($conn, $_POST['firstname']);
        $lastname = mysqli_escape_string($conn, $_POST['lastname']);
        $phone = mysqli_escape_string($conn, $_POST['phone']);
        $password = mysqli_escape_string($conn, $_POST['password']);
        $password_repeat = mysqli_escape_string($conn, $_POST['password_repeat']);

        //1. validate format
        $isCorrectEmail = false;
        $isCorrectname = false;
        $isCorrectPhone  = false;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $output = 'Invalid email format';
            echo $output;
        } else {
            $isCorrectEmail = true;
        }
        if ($isCorrectEmail == true) {
            if (!preg_match("/^[a-zA-Z ]*$/", $firstname) || !preg_match("/^[a-zA-Z ]*$/", $firstname)) {
                $output = 'Invalid Name';
                echo $output;
            } else {
                $isCorrectname = true;
            }
        }
        if ($isCorrectname == true) {
            if (!preg_match("/^[0-9]*$/", $phone)) {
                $output = 'Invalid phone number';
                echo $output;
            } else {
                $isCorrectPhone = true;
            }
        }

        // insertDB_customer
        if ($isCorrectPhone  == true) {
            // check email address if registered
            $statement = $conn->stmt_init();
            $sql = "select * from customer where customerEmail = ?;";
            if (!$statement->prepare($sql)) {
                die('prepare() failed: ' . $conn->error);
            }
            if (!$statement->bind_param("s", $email)) {
                die('bind_param() failed: ' . $statement->error);
            }
            if (!$statement->execute()) {
                die('execute() failed: ' . $statement->error);
            }
            $statement->store_result();
            $numofrows = $statement->num_rows;
            $hasRegistered = false;
            if ($numofrows > 0) {
                $hasRegistered = true;
                $output = 'This email address has been registered. Please try another email address';
                echo $output;
            }

            if ($hasRegistered == false) {
                $isMismatchedpassword = true;
                //check if password_repeat matches password
                if ($password == $password_repeat) {
                    $isMismatchedpassword = false;
                }
                if ($isMismatchedpassword == true) {
                    $output = 'passwords do not match';
                    echo $output;
                } else {
                    // insert into database_customer
                    $sql = "insert into customer (customerEmail, firstName, lastName, password, phoneNumber) values(?,?,?,?,?)";
                    if (!$statement->prepare($sql)) {
                        die('prepare() failed: ' . $conn->error);
                    }
                    if (!$statement->bind_param("sssss", $email, $firstname, $lastname, $password, $phone)) {
                        die('bind_param() failed: ' . $statement->error);
                    }
                    if (!$statement->execute()) {
                        die('execute() failed: ' . $statement->error);
                    }
                    $statement->store_result();
                    $affectedrows = $statement->affected_rows;
                    if ($affectedrows > 0) {
                        $output = 'Sign up success';
                        echo $output;
                        exit();
                    }else{
                        $output = 'Sign up failed';
                        echo $output;
                    }
                }
            }
        }
    }
} else {
    echo 'nice to meet you';
}
