<?php
$output = '';
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'insertDB_goods') {
        // connects to db
        require_once("../connections/conn.php");
        // declare php variable
        $goodsName = mysqli_escape_string($conn, $_POST['goodsName']);
        $stockPrice = mysqli_escape_string($conn, $_POST['stockPrice']);
        $remainingStock = mysqli_escape_string($conn, $_POST['remainingStock']);
        $consignmentStoreID = mysqli_escape_string($conn, $_POST['consignmentStoreID']);


        //1. validate input
        $isCorrectQuantity = false;
        $isCorrectPrice = false;
        $isCorrectConsignmentID = false;
        if (!preg_match('/^[0-9]+(\.[0-9])?$/', $stockPrice)) {
            $output = 'Price neither be integer or one digit decimal';
            echo $output;
        } else {
            $isCorrectPrice = true;
        }

        if ($isCorrectPrice) {
            if (!preg_match('/^[0-9]+$/', $remainingStock)) {
                $output = 'Wrong quantity input';
                echo $output;
            } else {
                $isCorrectQuantity = true;
            }
        }
        // }

        /* #region  insert into DB */
        if ($isCorrectQuantity) {
            $sql = "insert into goods(consignmentStoreID,goodsName,stockPrice,remainingStock,status) values (?,?,?,?,1)";
            $statement = $conn->stmt_init();
            if (!$statement->prepare($sql)) {
                die('prepare() failed: ' . $conn->error);
            }
            if (!$statement->bind_param("isdi", $consignmentStoreID, $goodsName, $stockPrice, $remainingStock)) {
                die('bind_param() failed: ' . $statement->error);
            }
            if (!$statement->execute()) {
                die('execute() failed: ' . $statement->error);
            }
            $statement->store_result();
            $affectedrows = $statement->affected_rows;
            $output = "success";
            echo $output;
            $statement->close();
        }
        /* #endregion */
    }
} else {
    echo 'nice to meet you';
}
