<?php 
$output='';
$action ='';
if(isset($_POST['action'])){
    $action=$_POST['action'];
}

if($action == 'removeGoods'){
    //connect to db
    require_once("../connections/conn.php");

    //declare php var
    $goodsNumber = mysqli_escape_string($conn, $_POST['goodsNumber']);
    $status = mysqli_escape_string($conn, $_POST['status']);
    if($status == 1){
        $status = 2;
    }else{
        $status = 1;
    }

    //update DB
    $sql = "update goods SET status = ? WHERE goodsNumber = ?; ";
    $statement = $conn->stmt_init();
    if(!$statement->prepare($sql)){
        die('prepare() failed: ' . $conn->error);
    }
    if (!$statement->bind_param("ii",$status, $goodsNumber)) {
        die('bind_param() failed: ' . $statement->error);
    }
    if(!$statement->execute()){
        die('execute() failed: ' . $statement->error);
    }
    $statement->store_result();
    $affectedrows = $statement->affected_rows;
    $output = "Status Changed";
    echo $output;
    $statement->close();
    // }
}else{
    $output = 'nice to meet you';
    echo $output;
}
?>