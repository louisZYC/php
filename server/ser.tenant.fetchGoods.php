<?php 

session_start();
$output ='';
if(isset($_POST["action"])){
require_once('../connections/conn.php');
// define php variable;
$tenantID = mysqli_escape_string($conn, $_POST['tenantID']);
    $procedure ="
        CREATE PROCEDURE fetchGoodsForTenant(in tenant_id varchar(50))
        BEGIN   
        select goodsNumber, goodsName, stockPrice, remainingStock, status, c.consignmentStoreID 
        FROM goods as g 
        INNER JOIN consignmentstore as c on c.consignmentStoreID = g.consignmentStoreID 
        INNER JOIN tenant as t on t.tenantID = c.tenantID
        WHERE t.tenantID = tenant_id;
        END;
    ";
    if(mysqli_query($conn,"drop procedure if exists fetchGoodsForTenant")){
        if(mysqli_query($conn, $procedure)){
            $SQL = 'Call fetchGoodsForTenant("'.$tenantID.'");';
            $result = mysqli_query($conn,$SQL);
            $output .='
          </div>
          
                <table class="datagripview">
                    <tr>
                        <th class="goodlist_th">Goods Number</th>
                        <th class="goodlist_th">goodsName</th>
                        <th class="goodlist_th">stockPrice</th>
                        <th class="goodlist_th">remainingStock</th>
                        <th class="goodlist_th">Store ID</th>
                        <th class="goodlist_th">Availability</th>
                        <th class="goodlist_th">Edit</th>
                        <th class="goodlist_th">Remove</th>
                    </tr>
            ';
            if(mysqli_num_rows($result)>0){
                while($row = mysqli_fetch_array($result)){
                     $output .='
                        <tr>
                            <td class="goodlist_td">'.$row["goodsNumber"].'</td>
                            <td class="goodlist_td">'.$row["goodsName"].'</td>
                            <td class="goodlist_td">'.$row["stockPrice"].'</td>
                            <td class="goodlist_td">'.$row["remainingStock"].'</td>
                            <td class="goodlist_td">'.$row["consignmentStoreID"].'</td>
                            ';
                            if($row['status']==1){
                                $output.='<td class="goodlist_td">true</td>';
                            }else{
                                $output.='<td class="goodlist_td">False</td>';
                            }
                            $output.='
                                <td class="goodlist_td">
                                  <div class="abc">
                                  <button 
                                    class="editGoods btn btn-danger" 
                                    type="button" 
                                    data-product_name="'.$row["goodsName"].'" 
                                    data-product_price="'.$row["stockPrice"].'" 
                                    data-product_id="'.$row["goodsNumber"].'" 
                                    data-product_stockquantity="'.$row["remainingStock"].'" 
                                    id='.$row["goodsNumber"].'>Edit
                                   </button>
                                   </div>
                                </td>
                                <td class="goodlist_td">
                                  <div class="abc">
<svg
data-status="'.$row["status"].'" data-goodsnum="'.$row["goodsNumber"].'"   style="cursor: pointer;" 
width="1.5em" height="1.5em" viewBox="0 0 16 16" class="removeGoods bi bi-arrow-left-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M10.146 7.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L12.793 11l-2.647-2.646a.5.5 0 0 1 0-.708z"/>
  <path fill-rule="evenodd" d="M2 11a.5.5 0 0 1 .5-.5H13a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 11zm3.854-9.354a.5.5 0 0 1 0 .708L3.207 5l2.647 2.646a.5.5 0 1 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 0 1 .708 0z"/>
  <path fill-rule="evenodd" d="M2.5 5a.5.5 0 0 1 .5-.5h10.5a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
</svg>
                                   </div>
                                </td>
                            </tr>
                            ';
                }

            }else{
                $output .='
                    <tr>
                        <td colspan="6">store of empty goods</td>
                    </tr>
                ';
            }
            $output .= '</table>';
            echo $output;
        }

    }
}else{
    $output= 'nice to meet you';
    echo $output;
}
