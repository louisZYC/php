  
<html>
<head>
<title>Hong Kong Cube Shopping System</title>
<!-- ajax css js library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
<!-- font library -->
<link href="https://fonts.googleapis.com/css2?family=Lato&family=Ubuntu&family=Volkhov&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Pangolin&display=swap" rel="stylesheet">
<!--   
		font-family: 'Ubuntu', sans-serif;
		font-family: 'Pangolin', cursive;
		font-family: 'Volkhov', serif;

  -->
<link rel="stylesheet" href="CSS/LoginUI.css" />
</head>
	
<body> 


<div class="loginFormcontainer">
  <div class="loginForm" style="height: 70%;">
    <form action="server/signup.php" method="POST" class="elements">
      <div id="btnback" style=" width : 10%; font-size: 30px;" ><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
      <path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/>
      <path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/> 
      </svg></div>
      <h2 style="margin-top: 0px;">Sign Up</h2>
      <div id="labelDiv" style="display: block; width:100%;">
      <label for="email" class="labelcss">Email</label>
      <label for="firstname" class="labelcss">First Name</label>
      <label for="lastname" class="labelcss">Last Name</label>
      <label for="phone" class="labelcss">Phone Number</label>
      <label for="password" class="labelcss">Password</label>
      <label for="password_repeat" class="labelcss">Re-password</label></div>
      <input class="inputbox" type="email" placeholder="Email" id="email" style="height: 7%;"><br>
      <input class="inputbox" type="text" placeholder="First Name" id="firstname" style="height: 7%;"><br>
      <input class="inputbox" type="text" placeholder="Last Name" id="lastname" style="height: 7%;"><br>
      <input class="inputbox" type="text" placeholder="Phone Number" id="phone" style="height: 7%;"><br>
      <input class="inputbox" type="password" placeholder="Password" id="password" style="height: 7%;"><br>
      <input class="inputbox" type="password" placeholder="Re-enter Password" id="password_repeat" style="height: 7%;"><br>
      <button type="button" id="submit_signup" value="signup" class="btn btn-primary btn-lg" style="display: block;">Sign up</button>
      <div class="errormessage" style="font-size: 20px;"><span id="errormessage"></span></div>
    </form>
  </div>
</div>

</body>
</html>


<script type="text/javascript">
  $(document).ready(function() {
    // signup button event
    $('#submit_signup').click(function() {
      var email = $('#email').val();
      var firstname = $('#firstname').val();
      var lastname = $('#lastname').val();
      var phone = $('#phone').val();
      var password = $('#password').val();
      var password_repeat = $('#password_repeat').val();
      // alert(email+firstname+lastname+phone+password+password_repeat);
      if ($.trim(email).length > 0 && $.trim(firstname).length > 0 && $.trim(lastname).length > 0 && $.trim(phone).length > 0 && $.trim(password).length > 0 && $.trim(password_repeat).length > 0) {
        $('#errormessage').html("1");
        $.ajax({
          url:"server/ser.signup.php",
          method: "POST",
          data:{
            action: "signup",
            email:email,
            firstname:firstname,
            lastname:lastname,
            phone:phone,
            password:password,
            password_repeat:password_repeat
          },
          beforeSend:function(){
            $('#submit_signup').html("running");
          },
          success:function(data){
            $('#errormessage').html(data);
            $('#submit_signup').html("Sign up");
            // if(data =='Sign up success'){
            //   $('input').val("");
            // }
          }
        })
      } else {
        $('#errormessage').html("Please fill in all fields");
      }

    })

  });

  $('#btnback').click(function(){
    window.location.href='loginUI.php';
  })
  
  $('#btnback').hover(function(){
    $(this).css('cursor', 'pointer');
  })
</script>